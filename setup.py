from setuptools import setup

setup(
    name='bnbsdkdistribution',
    version='0.0.2',
    packages=['bnbsdkdistribution'],
    url='',
    license='',
    author='antonkustov',
    author_email='',
    description='',
    install_requires=['dmgbuild', 'vswhere', 'biplist'],
)
