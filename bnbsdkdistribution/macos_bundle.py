from pathlib import Path
from . import otool
from . import install_name_tool
from typing import Iterable, Tuple, Optional
import typing
import platform
import subprocess

if platform.system() != "Darwin":
    raise ImportError(f'"{__name__}" implemented only  for MacOS')

LibDep = typing.NamedTuple('LibDep', [('link', Path), ('path', Path)])

_correct_folder = ["@loader_path/../Frameworks", "@executable_path/../Frameworks"]
_set_to = Path("@loader_path/../Frameworks")


def try_call(args: Iterable) -> Tuple[bool, Optional[str]]:
    try:
        o = subprocess.check_output([
            str(x.absolute()) if isinstance(x, Path)
            else str(x)
            for x in args if x is not None],
            stderr=subprocess.PIPE)
    except subprocess.CalledProcessError as ex:
        print(ex.cmd)
        return False, ex.stderr.strip().decode("utf-8")
    return True, o.strip().decode("utf-8")


def macdeployqt(qt_dir: Path, bundle: Path, binary: Path, deploy_plugins: bool,
                dmg: bool, qmldir: Path, libpath: Path):
    """Makes recursive copy and links changes"""
    tool = Path(qt_dir) / 'bin' / 'macdeployqt'

    if not tool.is_file():
        raise FileNotFoundError(f'Tool not found: {tool}')

    print('Running macdeployqt... ' + str(binary))

    cmd = [tool, bundle,
           # NOTE: do not use '-always-overwrite' option to avoid such bug for plugins:
           # NOTE: "'libqtquick2plugin.dylib' is not a valid Mach-O binary (not a dynamic library)"
           f'-executable={binary.absolute()}',
           '-no-plugins' if not deploy_plugins else None,
           '-dmg' if dmg else None,
           f'-qmldir={qmldir.absolute()}' if qmldir is not None else None,
           f'-libpath={libpath.absolute()}' if libpath is not None else None]

    ok, err_out = try_call(cmd)
    if not ok:
        print("macdeployqt call failed.")

    warning = []
    error = []

    if len(err_out) > 0:
        for l in err_out.split('\n'):
            check = l.strip().upper()
            if check.startswith('WARNING') and len(check.replace('WARNING:', '').strip()) > 0:
                warning.append(l.strip())
            elif check.startswith('ERROR'):
                error.append(l.strip())

    if len(warning) > 0:
        print('\n'.join(warning))
    if len(error) > 0:
        raise Exception("macdeployqt call failed:\n" + '\n'.join(error))
    pass


def _is_skip(link_path: Path) -> bool:
    return str(link_path.parent) in _correct_folder


def _get_libs_in_folder(folder: Path) -> typing.List[Path]:
    return [Path(d) for d in folder.iterdir() if Path(d).suffix == '.dylib']


def _get_depend_libs(binary_path: Path) -> typing.List[Path]:
    return [Path(d) for d in otool.list_dependencies(str(binary_path)) if Path(d).suffix == '.dylib']


def _match_deps_in_folder(folder: Path, binary_path: Path) -> typing.Generator[LibDep, None, None]:
    bundle_libs = _get_libs_in_folder(folder)

    for lib_link in _get_depend_libs(binary_path):
        lib_path = folder / lib_link.name
        if lib_path in bundle_libs:
            yield LibDep(lib_link, lib_path)


def _fix_link(binary_path: Path, link: Path):
    new_link = _set_to / link.name
    print(f"Fixing:\n"
          f"\told_link: {link}\n"
          f"\tnew_link: {new_link}")
    install_name_tool.change(binary_path.absolute(),
                             str(link),
                             str(new_link))
    pass


def _change_binary_links_recursively(folder: Path, binary_path: Path, fixed_list: typing.List[Path] = list()):
    print(f"Fixing bin: {binary_path.name}")

    fixed_list.append(binary_path)

    dep_libs = _match_deps_in_folder(folder, binary_path)
    for dep in dep_libs:

        # Skip self linking
        if binary_path.name == dep.link.name:
            continue

        # Skip already fixed binaries. Prevents endless cycle
        if dep.path not in fixed_list:
            _change_binary_links_recursively(folder, dep.path)

        # Skip already correct links
        if not _is_skip(dep.link):
            _fix_link(binary_path, dep.link)


def fix_bundle_binary_links(folder: str, binary_path: str):
    """
    :param folder: Path to "Frameworks" folder inside the bundle
    :param binary_path: Path to executable inside the bundle
    """
    binary = Path(binary_path)
    deps_dir = Path(folder)

    if not binary.is_file():
        raise FileNotFoundError('File not found: ' + binary_path)

    if not deps_dir.is_dir():
        raise NotADirectoryError('Folder not found: ' + folder)

    _change_binary_links_recursively(deps_dir, binary)
