from pathlib import Path, PurePath
import platform
import dmgbuild
import vswhere
import subprocess
from typing import Union, List, Mapping, Any, Iterable

fileloc = Union[PurePath, str]

if platform.system() == 'Darwin':
    from .macos_bundle import fix_bundle_binary_links, macdeployqt

    def complete_macos_bundle(macos_bundle_path, qt_dir, binary=None,
                              deploy_plugins=False, dmg=False, qmldir=None, libpath=None):
        """
        Copies dependencies to "Frameworks" folder and changes links to them.

        :param macos_bundle_path: Bundle path
        :param qt_dir: Qt installation dir. Used to find "macdeployqt"
        :param binary: Path to main executable inside the bundle. Bundle name will be used if not set.
        :param deploy_plugins: Set to False to skip plugin deployment
        :param dmg: Create a .dmg disk image
        :param qmldir: Deploy imports used by .qml files in the given path
        :param libpath: Add the given path to the library search path
        """

        bundle = Path(macos_bundle_path)
        binary = Path(binary) if binary is not None else \
            Path(macos_bundle_path) / 'Contents' / 'MacOS' / Path(macos_bundle_path).stem

        if not bundle.is_dir():
            raise FileNotFoundError(f'Bundle not found: {bundle}')

        # note: copy libraries from "/usr/local"
        macdeployqt(qt_dir, bundle, binary, deploy_plugins, dmg, qmldir, libpath)

        # TODO: check with bnb studio build is this call necessary
        # fix_bundle_binary_links(str(bundle / 'Contents' / 'Frameworks'), str(binary))

    def create_dmg(bundle: fileloc, volume: str, icon: fileloc=None):
        dmgbuild.build_dmg(
            filename=str(bundle.parent / volume),
            volume_name=str(volume),
            settings={
                'files': [str(bundle)],
                'symlinks': {'Applications': '/Applications'},
                'icon': str(icon),
                'show_icon_preview': True,
                'window_rect': ((100, 100), (500, 280)),
                'icon_locations': {
                    bundle.name: (125, 90),
                    'Applications': (375, 90)
                }
            })

    def update_plist(plist: str, update_data: Mapping[str, str], rm_keys: Iterable[str]):
        import biplist
        plist_data = biplist.readPlist(str(plist))
        for k in rm_keys:
            plist_data.pop(k, None)
        plist_data.update(update_data)
        biplist.writePlist(plist_data, str(plist), binary=False)


if platform.system() == 'Windows':
    MSVC_2017 = '[15.0,16.0)'

    MSVC_KEY_DISPLAY_NAME = 'displayName'
    MSVC_KEY_INSTALLATION_PATH = 'installationPath'
    MSVC_KEY_INSTALLATION_VERSION = 'installationVersion'

    def find_msvc(version=MSVC_2017) -> List[Mapping[str, Any]]:
        # By default library uses '-utf8' argument and some installed versions of vswhere can be without this argument
        vswhere.DEFAULT_PATH = None
        return vswhere.find(version=version)


def qmake_query(qmake: fileloc) -> Mapping[str, str]:
    o = subprocess.check_output([str(qmake), '-query'])
    o = o.decode('utf-8')
    ret = {}
    for l in o.split('\n'):
        sp = l.strip().split(':', maxsplit=1)
        if len(sp) < 2:
            continue
        ret[sp[0]] = sp[1]
    return ret
