import subprocess
import logging

log = logging.getLogger('otool')

_tool = '/usr/bin/otool'


def list_dependencies(binary_path: str):
    o = subprocess.Popen([_tool, '-L', binary_path], stdout=subprocess.PIPE)
    for line in o.stdout:
        line = line.decode("utf-8")
        if line[0] == '\t':
            yield line.split(' ', 1)[0][1:]
