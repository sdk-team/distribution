import subprocess
import logging

log = logging.getLogger('install_name_tool')

_tool = '/usr/bin/install_name_tool'


def change(binary_path: str, old: str, new: str):
    try:
        subprocess.run([_tool, '-change', old, new, binary_path],
                       stdout=subprocess.PIPE,
                       stderr=subprocess.STDOUT,
                       check=True)
    except subprocess.CalledProcessError as e:
        raise Exception("install_name_tool call failed. " + e.output.decode('utf-8'))
