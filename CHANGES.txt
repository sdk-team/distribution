## [0.0.2] - not released
### Added
- vswhere for winOS.
- qmake_query

## [0.0.1] - 2018-06-11 -- Initial release.
